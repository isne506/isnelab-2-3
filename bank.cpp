#include<iostream>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<algorithm>

using namespace std;

void printVector(vector<int> );
void print(int,vector<int>,vector<int>,vector<int>);

int main(){
	srand(time(0)) ;
	int maxCustomer , cashierNum = 1, minCustomer , 
		maxServiceTime , minServiceTime , customerNum;
	float avrWaitTime ;	
	
	vector<int> arriveTime;
	vector<int> leftTime;
	vector<int> serviceTime;
	vector<int> waitTime;

	cout << "Enter min customer : ";
	cin >> minCustomer; 				//collect min customer
	cout << "\nEnter max customer : ";
	cin >> maxCustomer;					//collect max customer

	
	customerNum = (rand() % (maxCustomer + 1 - minCustomer)) + minCustomer ;
	cout << "\ncustomer number is : " << customerNum; // random customer from max and min
	
	cout << "\nEnter min service time : ";
	cin >> minServiceTime;					//add min service time 
	cout << "\nEnter max service time : ";
	cin >> maxServiceTime;					//add max service time
	
	cout<<"\nEnter cashier number : ";
	cin >> cashierNum ; // add number of cashier
	
	for(int i=0;i<customerNum;i++){
		serviceTime.push_back((rand()%(minServiceTime + 1 - maxServiceTime) + minServiceTime )); // random service time length
	}
	
	for(int i=0;i<customerNum;i++){
		arriveTime.push_back(rand()%241) ; //random arrive time
	}	
	sort(arriveTime.begin(),arriveTime.end()) ; //sort arrive time as queue
	
	for(int i=0;i<customerNum;i++){
		leftTime.push_back(arriveTime[i]+serviceTime[i]); //add left time
	}
    
   
		for(int i=0;i<customerNum;i++){
			if(i<cashierNum){
				waitTime.push_back(0); //when it is the first customer of cashier NO waiting time
			}
			else{
				if(leftTime[i-cashierNum] - arriveTime[i] < 0){
					waitTime.push_back(0); //if come after previous customer left no wait time
				}
				else{
			 		waitTime.push_back(leftTime[i-cashierNum] - arriveTime[i] ); // wait for previous customer
				}
			}
		}	
	
	
	cout << "\nCustomers arrive time is : " ;
	printVector(arriveTime);
	cout << "\nService time for each customer is :  ";
	printVector(serviceTime);
	cout << "\nLeft time for each customer is : ";
	printVector(leftTime);
	cout << "\nWait time for each customer is : ";
	printVector(waitTime); 
	cout << endl;
	print(customerNum,arriveTime,waitTime,leftTime);  //all of this is about printing vector
	
	for(int i=0;i<waitTime.size();i++){
		avrWaitTime += waitTime[i] ;  //collect all wait time
	}
	
	cout << "\n\n Average waiting time is : " << avrWaitTime/waitTime.size() << " minute" ; // calculate average wait time
	
	return 0;
}

void printVector(vector<int> vector){
	for(int i=0;i < vector.size(); i++){
		cout << vector[i] << " ";
	}
}

void print(int customerNum,vector<int> arriveTime,vector<int> waitTime,vector<int>leftTime){
	for(int i=0;i<customerNum;i++){
		cout<<"customer "<< i+1 ;
		cout<<" - Arrive time : "<< arriveTime[i];
		cout<<" - Wait time : "<< waitTime[i];
		cout<<" - Left time : "<< leftTime[i];
		cout<< endl ; 
	}
}


